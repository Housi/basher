import styled from "styled-components";

const HeaderImage = styled.img`
  width: 100px;
  height: 100px;
  object-fit: cover;
`;

export default HeaderImage;
