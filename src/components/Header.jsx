import React, { useContext } from "react";
import styled from "styled-components";
import { A } from "hookrouter";
import Icon from "components/ui/Icon";
import { UserContext } from "data/UserContext";
import { DEFAULT_AVATAR_URL } from "config";

const Navbar = styled.nav`
  background-color: var(--fg-color);
  padding: var(--space);
  border-bottom: var(--box-border);
  display: flex;
  justify-content: space-between;
`;

const Logo = styled.span`
  font-family: "PexelGrotesk", monospace;
  font-size: 24px;
`;

const Image = styled.img`
  width: 40px;
  border-radius: 50%;
`;

const Header = () => {
  const auth = useContext(UserContext);

  return (
    <Navbar>
      <div>
        <A href="/settings">
          <Image src={auth.user.photoUrl || DEFAULT_AVATAR_URL} alt="user" />
        </A>
      </div>
      <A href="/">
        <Logo>
          <Icon name="smile" />
          BASH
        </Logo>
      </A>

      <div>
        <A href="/chats">
          <Icon name="mail" />
        </A>
      </div>
    </Navbar>
  );
};

export default Header;
