import React, { useContext } from "react";
import styled from "styled-components";
import { useForm } from "react-hook-form";
import { useTranslation } from "react-i18next";

import Textarea from "components/ui/Textarea";
import Button from "components/ui/Button";
import { chatRequest } from "data/ChatService";
import { UserContext } from "data/UserContext";

const StyledTextarea = styled(Textarea)`
  border: 2px solid red;
  flex: 1 100%;
`;

const FormWrapper = styled.form`
  display: flex;
`;

const ChatRequestForm = ({ userIdTo, onClose }) => {
  const { register, handleSubmit } = useForm();
  const { t } = useTranslation();

  const { uid, user } = useContext(UserContext);
  const onSubmit = (data) => {
    chatRequest({
      ...data,
      userIdTo,
      userIdFrom: uid,
      userProfileFrom: { id: uid, ...user },
    });
    onClose();
  };

  return (
    <FormWrapper onSubmit={handleSubmit(onSubmit)}>
      <StyledTextarea
        name="message"
        defaultValue="Hey whats the details"
        ref={register({ required: true })}
      />
      <div>
        <Button onClick={handleSubmit(onSubmit)}>{t("send")}</Button>
        <Button onClick={onClose}>{t("cancel")}</Button>
      </div>
    </FormWrapper>
  );
};

export default ChatRequestForm;
