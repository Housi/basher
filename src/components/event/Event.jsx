import React, { useState } from "react";
import styled from "styled-components";
import { useTranslation } from "react-i18next";

import HeaderImage from "components/user/HeaderImage";
import Button from "components/ui/Button";
import ChatRequestForm from "components/event/ChatRequestForm";
import { getAge, formatDistanceToNow } from "utils/datefns";

const UserInfo = styled.p`
  margin-block-start: 0;
  margin-block-end: 0.4em;
  font-weight: bold;
`;

const CardActions = styled.div``;

const CardMain = styled.div`
  flex: 1 100%;
  padding: 0 var(--space);
`;

const EventMiniCard = styled.div`
  display: flex;
  background-color: var(--fg-color);
  padding: var(--space);
  border: var(--box-border);
  margin-bottom: 1px;
`;

const Time = styled.span`
  background: var(--bg-color);
`;

const Event = ({ event }) => {
  const { date, description, user, userId } = event;
  const userAge = getAge(user.birthday);
  const eventTime = formatDistanceToNow(new Date(date));
  const { t } = useTranslation();
  const [showForm, setShowForm] = useState(false);

  const CardContent = () => (
    <>
      <HeaderImage src={user.photoUrl} />

      <CardMain>
        <Time>
          {t("event in")} {eventTime}
        </Time>
        <UserInfo>
          {user.name} 🎉 {userAge}
        </UserInfo>
        <p>{description}</p>
      </CardMain>

      <CardActions>
        <Button onClick={() => setShowForm(true)}>{t("join")}</Button>
      </CardActions>
    </>
  );

  return (
    <EventMiniCard>
      {showForm ? (
        <ChatRequestForm onClose={() => setShowForm(false)} userIdTo={userId} />
      ) : (
        <CardContent />
      )}
    </EventMiniCard>
  );
};

export default Event;
