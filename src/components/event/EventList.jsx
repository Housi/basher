import React from "react";
import styled from "styled-components";
import Event from "components/event/Event";

const Container = styled.div`
  padding: var(--space);
`;

const EventList = ({ events }) => {
  return (
    <Container>
      {events.map((event) => {
        return <Event event={event} key={event.userId} />;
      })}
    </Container>
  );
};

export default EventList;
