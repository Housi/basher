import React, { forwardRef } from "react";
import styled from "styled-components";

const TextAreaBody = styled.textarea`
  padding: var(--space);
  margin-bottom: var(--bash-margin);
  width: 100%;
`;

const TextArea = forwardRef(({ name, type, defaultValue }, ref) => (
  <TextAreaBody ref={ref} name={name} type={type} defaultValue={defaultValue} />
));

export default TextArea;
