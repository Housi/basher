import React from "react";
import { useTranslation } from "react-i18next";
import styled from "styled-components";

const ErrorInfo = styled.div`
  margin: -15px 0 -15px 0;
  color: var(--bash-error);
`;

const FormError = () => {
  const { t } = useTranslation();
  return <ErrorInfo>{t("field required")}</ErrorInfo>;
};

export default FormError;
