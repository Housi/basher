import React, { forwardRef } from "react";
import styled from "styled-components";

const StyledInput = styled.input`
  padding: var(--space);
  margin-top: var(--space);
`;

const Input = forwardRef(({ name, type, defaultValue }, ref) => (
  <StyledInput ref={ref} name={name} type={type} defaultValue={defaultValue} />
));

export default Input;
