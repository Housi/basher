import firebase from "firebase/app";
import "firebase/auth";
import "firebase/firestore";
import "firebase/storage";

import * as firebaseui from "firebaseui";

const config = {
  apiKey: process.env.REACT_APP_API_KEY,
  authDomain: process.env.REACT_APP_AUTH_DOMAIN,
  databaseURL: process.env.REACT_APP_DATABASE_URL,
  projectId: process.env.REACT_APP_PROJECT_ID,
  storageBucket: process.env.REACT_APP_STORAGE_BUCKET,
  messagingSenderId: process.env.REACT_APP_MESSAGING_SENDER_ID,
};

class Firebase {
  constructor() {
    firebase.initializeApp(config);
    this.auth = firebase.auth();
    this.ui = new firebaseui.auth.AuthUI(this.auth);
    this.database = firebase.firestore();
    this.storage = firebase.storage();
  }

  // *** Auth API ***
  uiStart(container) {
    this.ui.start(container, {
      signInOptions: [
        {
          provider: firebase.auth.PhoneAuthProvider.PROVIDER_ID,
          recaptchaParameters: {
            size: "invisible",
            callback: function() {
              return false;
            },
          },
          defaultCountry: "PL",
        },
      ],
    });
  }
}

const fb = new Firebase();

export default fb;
