import React, { useContext } from "react";
import { useRoutes } from "hookrouter";
import styled from "styled-components";

import LoginPage from "pages/LoginPage";
import Home from "pages/Home";
import Chats from "pages/Chats";
import Chat from "pages/Chat";

import Header from "components/Header";
import ProfileForm from "components/user/ProfileForm";
import Settings from "pages/Settings";
import Loader from "components/ui/Loader";
import { UserContext } from "data/UserContext";

const Layout = styled.div`
  display: flex;
  height: 100vh;
  flex-direction: column;
`;

const Main = styled.main`
  flex: 1;
  width: 100%;
  max-width: var(--content-width);
  margin: 0 auto;
`;

const App = () => {
  const auth = useContext(UserContext);

  const routes = {
    "/": () => <Home />,
    "/settings": () => <Settings />,
    "/chats": () => <Chats uid={auth.uid} />,
    "/chat/:chatId": ({ chatId }) => <Chat chatId={chatId} />,
  };

  const routeResult = useRoutes(routes);
  if (!auth.ready) return <Loader />;
  if (!auth.uid) return <LoginPage />;

  return (
    <Layout>
      <Header />
      <Main>{auth.user.name ? routeResult : <ProfileForm />}</Main>
    </Layout>
  );
};

export default App;
