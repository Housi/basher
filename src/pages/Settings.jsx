import React, { useContext } from "react";
import ProfileForm from "components/user/ProfileForm";
import styled from "styled-components";
import Button from "components/ui/Button";
import { useTranslation } from "react-i18next";
import { UserContext } from "data/UserContext";

const Layout = styled.div`
  display: flex;
  min-height: 100%;
  flex-direction: column;
  justify-content: space-between;
  padding: var(--space);
`;

const Settings = () => {
  const { t } = useTranslation();
  const auth = useContext(UserContext);

  return (
    <Layout>
      <ProfileForm />
      <Button onClick={auth.logout}>{t("logout")}</Button>
    </Layout>
  );
};

export default Settings;
