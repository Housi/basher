import React, { useRef, useLayoutEffect } from "react";
import styled from "styled-components";
import fb from "data/firebase";

const Box = styled.div`
  padding: 2em;
  text-align: center;
`;

const LoginPage = () => {
  const authContainer = useRef(null);
  var userLang = navigator.language || navigator.userLanguage;

  useLayoutEffect(() => {
    fb.uiStart(authContainer.current);
  });

  return (
    <Box>
      <h2>Login</h2>
      <div lang={userLang} ref={authContainer} />
    </Box>
  );
};

export default LoginPage;
