import React, { useState, useEffect, useContext } from "react";
import styled from "styled-components";
import { useForm } from "react-hook-form";
import { UserContext } from "data/UserContext";
import { messageApi, sendMessage } from "data/MessageService";
import Textarea from "components/ui/Textarea";
import Button from "components/ui/Button";

const MessageBox = styled.div`
  position: fixed;
  bottom: 0;
  left: 0;
  right: 0;
  background: pink;
  padding: var(--space);
`;

const Form = styled.form`
  width: 100%;
  max-width: var(--content-width);
  margin: 0 auto;
  display: flex;
`;

const Chat = ({ chatId }) => {
  const [messages, setMessages] = useState([]);
  const { uid } = useContext(UserContext);
  const { register, handleSubmit } = useForm();
  const onSubmit = ({ message }) => {
    sendMessage({ message, chatId, userId: uid });
  };

  useEffect(() => {
    messageApi.doc(chatId).onSnapshot((doc) => {
      const msg = Object.values(doc.data());
      setMessages(msg);
    });
  }, [chatId]);

  return (
    <>
      <h2>superchat</h2>
      {messages.map((message, index) => (
        <p key={index}>{message.content}</p>
      ))}
      <MessageBox>
        <Form onSubmit={handleSubmit(onSubmit)}>
          <Textarea name="message" ref={register({ required: true })} />
          <Button onClick={handleSubmit(onSubmit)}>send</Button>
        </Form>
      </MessageBox>
    </>
  );
};

export default Chat;
