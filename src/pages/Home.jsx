import React, { useState, useContext, useEffect } from "react";
import styled from "styled-components";
import { UserContext } from "data/UserContext";
import { getEvents, Event } from "data/EventService";
import EventList from "components/event/EventList";
import EventEdit from "components/event/EventEdit";

const Layout = styled.div``;

const Drawer = styled.div`
  background-color: var(--yellow);
`;

const Home = () => {
  const { uid, user } = useContext(UserContext);
  const [events, setEvents] = useState([]);
  const [userEvent, setUserEvent] = useState(new Event(user, uid));

  useEffect(() => {
    async function getData() {
      const [eventsData, userEventsData] = await getEvents(uid);
      setEvents(eventsData);
      if (userEventsData.length) {
        setUserEvent(userEventsData[0]);
      }
    }
    getData();
  }, [uid]);

  return (
    <Layout>
      <EventList events={events} />
      <Drawer>
        <EventEdit event={userEvent} />
      </Drawer>
    </Layout>
  );
};

export default Home;
